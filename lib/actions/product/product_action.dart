import 'package:built_collection/built_collection.dart';
import 'package:data_api_cache/models/models.dart';

class GetProductList {}

class SaveProductList {
  final BuiltList<Product>? products;

  SaveProductList({this.products});
}