// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_connector.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AuthViewModel extends AuthViewModel {
  @override
  final GetUserDetailsAction getUserDetailsAction;
  @override
  final LoginWithPasswordAction loginWithPassword;
  @override
  final LogOutAction logOut;
  @override
  final UploadFileAction uploadFileAction;
  @override
  final AppUser? currentUser;
  @override
  final bool isInitializing;

  factory _$AuthViewModel([void Function(AuthViewModelBuilder)? updates]) =>
      (new AuthViewModelBuilder()..update(updates)).build();

  _$AuthViewModel._(
      {required this.getUserDetailsAction,
      required this.loginWithPassword,
      required this.logOut,
      required this.uploadFileAction,
      this.currentUser,
      required this.isInitializing})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        getUserDetailsAction, 'AuthViewModel', 'getUserDetailsAction');
    BuiltValueNullFieldError.checkNotNull(
        loginWithPassword, 'AuthViewModel', 'loginWithPassword');
    BuiltValueNullFieldError.checkNotNull(logOut, 'AuthViewModel', 'logOut');
    BuiltValueNullFieldError.checkNotNull(
        uploadFileAction, 'AuthViewModel', 'uploadFileAction');
    BuiltValueNullFieldError.checkNotNull(
        isInitializing, 'AuthViewModel', 'isInitializing');
  }

  @override
  AuthViewModel rebuild(void Function(AuthViewModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AuthViewModelBuilder toBuilder() => new AuthViewModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    final dynamic _$dynamicOther = other;
    return other is AuthViewModel &&
        getUserDetailsAction == _$dynamicOther.getUserDetailsAction &&
        loginWithPassword == _$dynamicOther.loginWithPassword &&
        logOut == _$dynamicOther.logOut &&
        uploadFileAction == _$dynamicOther.uploadFileAction &&
        currentUser == other.currentUser &&
        isInitializing == other.isInitializing;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc(0, getUserDetailsAction.hashCode),
                        loginWithPassword.hashCode),
                    logOut.hashCode),
                uploadFileAction.hashCode),
            currentUser.hashCode),
        isInitializing.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AuthViewModel')
          ..add('getUserDetailsAction', getUserDetailsAction)
          ..add('loginWithPassword', loginWithPassword)
          ..add('logOut', logOut)
          ..add('uploadFileAction', uploadFileAction)
          ..add('currentUser', currentUser)
          ..add('isInitializing', isInitializing))
        .toString();
  }
}

class AuthViewModelBuilder
    implements Builder<AuthViewModel, AuthViewModelBuilder> {
  _$AuthViewModel? _$v;

  GetUserDetailsAction? _getUserDetailsAction;
  GetUserDetailsAction? get getUserDetailsAction =>
      _$this._getUserDetailsAction;
  set getUserDetailsAction(GetUserDetailsAction? getUserDetailsAction) =>
      _$this._getUserDetailsAction = getUserDetailsAction;

  LoginWithPasswordAction? _loginWithPassword;
  LoginWithPasswordAction? get loginWithPassword => _$this._loginWithPassword;
  set loginWithPassword(LoginWithPasswordAction? loginWithPassword) =>
      _$this._loginWithPassword = loginWithPassword;

  LogOutAction? _logOut;
  LogOutAction? get logOut => _$this._logOut;
  set logOut(LogOutAction? logOut) => _$this._logOut = logOut;

  UploadFileAction? _uploadFileAction;
  UploadFileAction? get uploadFileAction => _$this._uploadFileAction;
  set uploadFileAction(UploadFileAction? uploadFileAction) =>
      _$this._uploadFileAction = uploadFileAction;

  AppUserBuilder? _currentUser;
  AppUserBuilder get currentUser =>
      _$this._currentUser ??= new AppUserBuilder();
  set currentUser(AppUserBuilder? currentUser) =>
      _$this._currentUser = currentUser;

  bool? _isInitializing;
  bool? get isInitializing => _$this._isInitializing;
  set isInitializing(bool? isInitializing) =>
      _$this._isInitializing = isInitializing;

  AuthViewModelBuilder();

  AuthViewModelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _getUserDetailsAction = $v.getUserDetailsAction;
      _loginWithPassword = $v.loginWithPassword;
      _logOut = $v.logOut;
      _uploadFileAction = $v.uploadFileAction;
      _currentUser = $v.currentUser?.toBuilder();
      _isInitializing = $v.isInitializing;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AuthViewModel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AuthViewModel;
  }

  @override
  void update(void Function(AuthViewModelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AuthViewModel build() {
    _$AuthViewModel _$result;
    try {
      _$result = _$v ??
          new _$AuthViewModel._(
              getUserDetailsAction: BuiltValueNullFieldError.checkNotNull(
                  getUserDetailsAction,
                  'AuthViewModel',
                  'getUserDetailsAction'),
              loginWithPassword: BuiltValueNullFieldError.checkNotNull(
                  loginWithPassword, 'AuthViewModel', 'loginWithPassword'),
              logOut: BuiltValueNullFieldError.checkNotNull(
                  logOut, 'AuthViewModel', 'logOut'),
              uploadFileAction: BuiltValueNullFieldError.checkNotNull(
                  uploadFileAction, 'AuthViewModel', 'uploadFileAction'),
              currentUser: _currentUser?.build(),
              isInitializing: BuiltValueNullFieldError.checkNotNull(
                  isInitializing, 'AuthViewModel', 'isInitializing'));
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'currentUser';
        _currentUser?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AuthViewModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
