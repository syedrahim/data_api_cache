import 'package:built_collection/built_collection.dart';
import 'package:localstorage/localstorage.dart';
import 'package:data_api_cache/actions/actions.dart';
import 'package:data_api_cache/models/models.dart';
import 'package:built_value/built_value.dart';
import 'package:flutter/material.dart' hide Builder;
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

part 'product_connector.g.dart'; 

typedef GetProductsListAction = void Function();


abstract class ProductViewModel
    implements Built<ProductViewModel, ProductViewModelBuilder> {
  factory ProductViewModel(
      [ProductViewModelBuilder Function(ProductViewModelBuilder builder)
      updates]) = _$ProductViewModel;

  ProductViewModel._();

  factory ProductViewModel.fromStore(Store<AppState> store) {
    return ProductViewModel((ProductViewModelBuilder b) {
      return b
        ..isInitializing = store.state.isInitializing
        ..isLoading = store.state.isLoading
        ..products = store.state.products?.toBuilder()
        ..getProductListAction = () {
          store.dispatch(GetProductList());
        };

    });
  }
  GetProductsListAction get getProductListAction;

  bool get isInitializing;

  bool get isLoading;

  BuiltList<Product>? get products;
}

class ProductConnector extends StatelessWidget {
  const ProductConnector({required this.builder});

  final ViewModelBuilder<ProductViewModel> builder;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ProductViewModel>(
      builder: builder,
      converter: (Store<AppState> store) => ProductViewModel.fromStore(store),
    );
  }
}
