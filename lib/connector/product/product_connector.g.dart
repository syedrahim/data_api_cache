// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_connector.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ProductViewModel extends ProductViewModel {
  @override
  final GetProductsListAction getProductListAction;
  @override
  final bool isInitializing;
  @override
  final bool isLoading;
  @override
  final BuiltList<Product>? products;

  factory _$ProductViewModel(
          [void Function(ProductViewModelBuilder)? updates]) =>
      (new ProductViewModelBuilder()..update(updates)).build();

  _$ProductViewModel._(
      {required this.getProductListAction,
      required this.isInitializing,
      required this.isLoading,
      this.products})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        getProductListAction, 'ProductViewModel', 'getProductListAction');
    BuiltValueNullFieldError.checkNotNull(
        isInitializing, 'ProductViewModel', 'isInitializing');
    BuiltValueNullFieldError.checkNotNull(
        isLoading, 'ProductViewModel', 'isLoading');
  }

  @override
  ProductViewModel rebuild(void Function(ProductViewModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProductViewModelBuilder toBuilder() =>
      new ProductViewModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    final dynamic _$dynamicOther = other;
    return other is ProductViewModel &&
        getProductListAction == _$dynamicOther.getProductListAction &&
        isInitializing == other.isInitializing &&
        isLoading == other.isLoading &&
        products == other.products;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, getProductListAction.hashCode), isInitializing.hashCode),
            isLoading.hashCode),
        products.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProductViewModel')
          ..add('getProductListAction', getProductListAction)
          ..add('isInitializing', isInitializing)
          ..add('isLoading', isLoading)
          ..add('products', products))
        .toString();
  }
}

class ProductViewModelBuilder
    implements Builder<ProductViewModel, ProductViewModelBuilder> {
  _$ProductViewModel? _$v;

  GetProductsListAction? _getProductListAction;
  GetProductsListAction? get getProductListAction =>
      _$this._getProductListAction;
  set getProductListAction(GetProductsListAction? getProductListAction) =>
      _$this._getProductListAction = getProductListAction;

  bool? _isInitializing;
  bool? get isInitializing => _$this._isInitializing;
  set isInitializing(bool? isInitializing) =>
      _$this._isInitializing = isInitializing;

  bool? _isLoading;
  bool? get isLoading => _$this._isLoading;
  set isLoading(bool? isLoading) => _$this._isLoading = isLoading;

  ListBuilder<Product>? _products;
  ListBuilder<Product> get products =>
      _$this._products ??= new ListBuilder<Product>();
  set products(ListBuilder<Product>? products) => _$this._products = products;

  ProductViewModelBuilder();

  ProductViewModelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _getProductListAction = $v.getProductListAction;
      _isInitializing = $v.isInitializing;
      _isLoading = $v.isLoading;
      _products = $v.products?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProductViewModel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ProductViewModel;
  }

  @override
  void update(void Function(ProductViewModelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProductViewModel build() {
    _$ProductViewModel _$result;
    try {
      _$result = _$v ??
          new _$ProductViewModel._(
              getProductListAction: BuiltValueNullFieldError.checkNotNull(
                  getProductListAction,
                  'ProductViewModel',
                  'getProductListAction'),
              isInitializing: BuiltValueNullFieldError.checkNotNull(
                  isInitializing, 'ProductViewModel', 'isInitializing'),
              isLoading: BuiltValueNullFieldError.checkNotNull(
                  isLoading, 'ProductViewModel', 'isLoading'),
              products: _products?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'products';
        _products?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProductViewModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
