import 'package:built_collection/built_collection.dart';
import 'package:data_api_cache/data/api/api_client.dart';
import 'package:data_api_cache/data/services/api_service.dart';
import 'package:data_api_cache/models/models.dart';

class ProductService extends ApiService {
  ProductService({required ApiClient client}) : super(client: client);

//**************************** get-Products-List *************************//
  Future<BuiltList<Product>?> getProductsList({Map<String, String>? headersToApi,
    Map<String, dynamic>? queryParams}) async {

    final ApiResponse<ApiSuccess> res = await client!.callJsonApi<ApiSuccess>(
      method: Method.GET,
      headers: headersToApi,
      queryParams: queryParams,
      path: '/api/users',
    );
    if (res.isSuccess) {
      return res.resData?.products;
    } else if (res.isUnAuthorizedRequest) {
      throw true;
    } else {
      throw res.error;
    }
  }
  
}
