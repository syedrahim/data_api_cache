import 'package:data_api_cache/data/app_repository.dart';
import 'package:data_api_cache/middleware/product/product_middleware.dart';
import 'package:data_api_cache/models/models.dart';
import 'package:data_api_cache/middleware/auth/auth_middleware.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';

EpicMiddleware<AppState> epicMiddleware(AppRepository repository) =>
    EpicMiddleware<AppState>(
      combineEpics<AppState>(
        <Epic<AppState>>[],
      ),
    );

List<Middleware<AppState>> middleware(AppRepository repository) =>
    <List<Middleware<AppState>>>[
      AuthMiddleware(repository: repository).createAuthMiddleware(),
      ProductMiddleware(repository: repository).createProductMiddleware(),
    ].expand((List<Middleware<AppState>> list) => list).toList();
