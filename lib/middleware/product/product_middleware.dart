import 'package:built_collection/built_collection.dart';
import 'package:localstorage/localstorage.dart';
import 'package:data_api_cache/actions/actions.dart';
import 'package:data_api_cache/data/app_repository.dart';
import 'package:data_api_cache/data/services/product/product_service.dart';
import 'package:data_api_cache/models/models.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class ProductMiddleware {
  ProductMiddleware({required this.repository})
      : productService =
  repository.getService<ProductService>() as ProductService;

  final AppRepository repository;
  final ProductService productService;
  static LocalStorage storage = new LocalStorage('products');

  List<Middleware<AppState>> createProductMiddleware() {
    return <Middleware<AppState>>[
      TypedMiddleware<AppState, GetProductList>(getProductList),
    ];
  }

//**************************** get-notifications-list *************************//
  void getProductList(Store<AppState> store, GetProductList action,
      NextDispatcher next) async {
    try {
      store.dispatch(SetLoader(true));
      BuiltList<Product>? products;

      final Map<String, String> queryParams = <String, String>{
        'page': '1',
      };
      products = await productService.getProductsList(
        queryParams: queryParams
      );
      print("----------------from API");
      store.dispatch(SaveProductList(products: products));
      store.dispatch(SetLoader(false));
    } on ApiError catch (e) {
      store.dispatch(SetLoader(false));
      print("--------------------------${e.errorMessage}");
      return;
    } catch (e) {
      store.dispatch(SetLoader(false));
      debugPrint('============ get products list catch block ========== ${e?.toString()}');
    }
    next(action);
  }
}
