import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'product.g.dart';

abstract class Product implements Built<Product, ProductBuilder> {
  factory Product([ProductBuilder updates(ProductBuilder builder)]) = _$Product;

  Product._();

  int? get id;

  @BuiltValueField(wireName: 'first_name')
  String? get title;

  @BuiltValueField(wireName: 'last_name')
  String? get description;

  int? get price;

  @BuiltValueField(wireName: 'email')
  String? get brand;

  String? get category;

  @BuiltValueField(wireName: 'avatar')
  String? get thumbnail;

  static Serializer<Product> get serializer => _$productSerializer;
}
