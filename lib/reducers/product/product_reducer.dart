import 'package:data_api_cache/actions/actions.dart';
import 'package:data_api_cache/models/app_state.dart';
import 'package:redux/redux.dart';

Reducer<AppState> productReducer = combineReducers(<Reducer<AppState>>[
  TypedReducer<AppState, SaveProductList>(saveProductList),
]);

AppState saveProductList(AppState state, SaveProductList action) {
  final AppStateBuilder b = state.toBuilder();
  b..products = action.products?.toBuilder();
  return b.build();
}
