import 'package:data_api_cache/models/models.dart';
import 'package:data_api_cache/reducers/auth/auth_reducer.dart';
import 'package:data_api_cache/reducers/notification/notification_reducer.dart';
import 'package:data_api_cache/reducers/product/product_reducer.dart';
import 'package:redux/redux.dart';

Reducer<AppState> reducer = combineReducers(<Reducer<AppState>>[
  authReducer,
  notificationReducer,
  productReducer
]);
