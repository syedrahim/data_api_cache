import 'package:flutter/material.dart';
import 'package:data_api_cache/connector/product/product_connector.dart';

class ProductListing extends StatelessWidget {
  const ProductListing({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProductConnector(
      builder: (BuildContext context, ProductViewModel productModel) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Product Listing'),
          ),
          body: productModel.isLoading == true ?
          CircularProgressIndicator() :
          productModel.products == null && productModel.products?.length == 0 ?
              Text('Empty List') :
          ListView.builder(
            itemCount: productModel.products?.length,
              itemBuilder: (context, index) => ListTile(
                title: Text('${productModel.products?[index].title}'),
                subtitle: Text('${productModel.products?[index].description}'),
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(productModel.products?[index].thumbnail ?? ""),
                  backgroundColor: Colors.blueAccent,
                ),
              )),
        );
      }
    );
  }
}
