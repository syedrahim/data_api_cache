import 'package:data_api_cache/connector/auth_connector.dart';
import 'package:data_api_cache/views/home/home_page.dart';
import 'package:data_api_cache/views/loader/app_loader.dart';
import 'package:data_api_cache/views/auth/login_page.dart';
import 'package:flutter/material.dart';

class InitPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AuthConnector(
      builder: (BuildContext c, AuthViewModel model) {
        return LoginPage();
        if (model.isInitializing) {
          return AppLoader();
        }
        return model.currentUser == null ? LoginPage() : HomePage();
      },
    );
  }
}
